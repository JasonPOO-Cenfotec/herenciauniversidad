package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Funcionario extends Persona{

    private double salario;
    private String departamento;
    private LocalDate fechaContratacion;

    public Funcionario() {
    }

    public Funcionario(String nombre, String identificacion, LocalDate fechaNacimiento, int edad, double salario, String departamento, LocalDate fechaContratacion) {
        super(nombre, identificacion, fechaNacimiento, edad);
        this.salario = salario;
        this.departamento = departamento;
        this.fechaContratacion = fechaContratacion;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    public String toString() {
        return super.toString() + ", salario=" + salario + ", departamento=" + departamento + ", fechaContratacion=" + fechaContratacion;
    }
}
