package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Estudiante extends Persona {

    private String carrera;
    private LocalDate fechaIngreso;

    public Estudiante() {
    }

    public Estudiante(String nombre, String identificacion, LocalDate fechaNacimiento, int edad, String carrera, LocalDate fechaIngreso) {
        super(nombre, identificacion, fechaNacimiento, edad);
        this.carrera = carrera;
        this.fechaIngreso = fechaIngreso;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }


    public String obtenerEstadoDeAnimo() {
        String estado = "";
        if(getEdad() > 20){
            estado = "Yo paso muy triste!";
        }else
            estado = "Yo la verdad no se como me siento!";
       return estado;
    }

    public String toString() {
        return  super.toString() + ", carrera=" + carrera+ ", fechaIngreso=" + fechaIngreso;
    }
}
