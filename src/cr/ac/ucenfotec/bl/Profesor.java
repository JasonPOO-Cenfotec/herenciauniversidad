package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Profesor extends Funcionario{

    private int aniosExperiencia;
    private String especialidad;
    private double tasaHora;
    private double horasTrabajadas;

    public Profesor() {
    }

    public Profesor(String nombre, String identificacion, LocalDate fechaNacimiento, int edad,
                    double salario, String departamento, LocalDate fechaContratacion,
                    int aniosExperiencia, String especialidad, double tasaHora, double horasTrabajadas) {
        super(nombre, identificacion, fechaNacimiento, edad, salario, departamento, fechaContratacion);
        this.aniosExperiencia = aniosExperiencia;
        this.especialidad = especialidad;
        this.tasaHora = tasaHora;
        this.horasTrabajadas = horasTrabajadas;
    }

    public int getAniosExperiencia() {
        return aniosExperiencia;
    }

    public void setAniosExperiencia(int aniosExperiencia) {
        this.aniosExperiencia = aniosExperiencia;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public double getTasaHora() {
        return tasaHora;
    }

    public void setTasaHora(double tasaHora) {
        this.tasaHora = tasaHora;
    }

    public double getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(double horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public String toString() {
        return super.toString() + ", aniosExperiencia=" + aniosExperiencia + ", especialidad='" +
                especialidad +", tasaHora=" + tasaHora + ", horasTrabajadas=" + horasTrabajadas;
    }
}
