package cr.ac.ucenfotec.ui;

import cr.ac.ucenfotec.bl.Estudiante;
import cr.ac.ucenfotec.bl.Funcionario;
import cr.ac.ucenfotec.bl.Persona;
import cr.ac.ucenfotec.bl.Profesor;

import java.time.LocalDate;

public class UI {
    public static void main(String[] args) {

        Persona persona1 = new Persona("Juan", "123", LocalDate.of(1985, 04, 19), 36);
        Persona persona2 = new Persona("Ana", "456", LocalDate.of(1990, 01, 02), 32);

        Estudiante estudiante1 = new Estudiante("Esteban", "1111", LocalDate.of(2000, 01, 01), 20, "Arquitectura", LocalDate.of(2010, 10, 11));
        Estudiante estudiante2 = new Estudiante("María", "2222", LocalDate.of(2000, 01, 01), 30, "Filosofía", LocalDate.of(2010, 10, 11));

        Funcionario funcionario1 = new Funcionario("Luis","333",LocalDate.of(1985,02,02),36,450000,"Contabilidad",LocalDate.of(2001,05,06));

        Profesor profesor1 = new Profesor("Andrea","444",LocalDate.of(1995,01,01),27,500000,"Ciencias",LocalDate.of(2020,01,01),5,"Botanica",3500,48);

        System.out.println("Persona1    : " + persona1.obtenerEstadoDeAnimo());
        System.out.println("Persona2    : " + persona2.obtenerEstadoDeAnimo());
        System.out.println("Estudiante1 : " + estudiante1.obtenerEstadoDeAnimo());
        System.out.println("Estudiante2 : " + estudiante2.obtenerEstadoDeAnimo());
        System.out.println("Funcionario1: " + funcionario1.obtenerEstadoDeAnimo());
        System.out.println("Profesor1   : " + profesor1.obtenerEstadoDeAnimo());
    }
}
